package hse.se.consumerprocessing

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class ConsumerProcessingApplication

fun main(args: Array<String>) {
	runApplication<ConsumerProcessingApplication>(*args)
}
