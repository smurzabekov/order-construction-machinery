package hse.se.priceprocessing

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PriceProcessingApplication

fun main(args: Array<String>) {
    runApplication<PriceProcessingApplication>(*args)
}
