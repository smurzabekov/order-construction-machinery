package hse.se.authgatewayapi.service

import hse.se.authgatewayapi.entity.User
import hse.se.authgatewayapi.repository.UserRepository
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserService(
    private val repository: UserRepository
) {
    fun findByEmail(email: String): User =
        repository.findByEmail(email)
            ?: throw UsernameNotFoundException("User with email = $email is not present!")

    fun save(user: User) = repository.save(user)
}