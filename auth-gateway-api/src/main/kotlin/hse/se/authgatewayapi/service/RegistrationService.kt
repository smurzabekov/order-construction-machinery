package hse.se.authgatewayapi.service

import hse.se.authgatewayapi.controller.dto.UserToRegistration
import hse.se.authgatewayapi.entity.Role
import hse.se.authgatewayapi.entity.RoleType
import hse.se.authgatewayapi.entity.User
import hse.se.authgatewayapi.repository.RoleRepository
import hse.se.authgatewayapi.repository.UserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class RegistrationService(
    private val userRepository: UserRepository,
    private val roleRepository: RoleRepository,
    private val passwordEncoder: PasswordEncoder
) {
    fun registerNewUser(user: UserToRegistration, roleType: RoleType): String {
        check(!userRepository.existsByUsername(user.username)) {
            "User with such username exists"
        }
        check(!userRepository.existsByEmail(user.email)) {
            "User with such email exists"
        }
        val role = roleRepository.findByName(roleType.fullName)
        val user = User(
            user.username,
            user.firstName,
            user.lastName,
            user.email,
            passwordEncoder.encode(user.password)
        ).apply {
            roles = mutableListOf<Role>(role)
        }
        userRepository.save(user)
        return "Registration successfully"
    }
}