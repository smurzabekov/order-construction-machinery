package hse.se.authgatewayapi.service

import hse.se.authgatewayapi.repository.RoleRepository
import org.springframework.stereotype.Service

@Service
class RoleService(
    private val repository: RoleRepository
) {
    fun findRoleByUserId(id: Long) = repository.findRolesByUsersId(id)
}