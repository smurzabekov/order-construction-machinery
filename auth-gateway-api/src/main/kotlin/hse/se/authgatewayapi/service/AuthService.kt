package hse.se.authgatewayapi.service

import hse.se.authgatewayapi.controller.dto.AuthenticationCredentialsDto
import hse.se.authgatewayapi.controller.dto.UserDto
import hse.se.authgatewayapi.exception.JwtAuthenticationException
import hse.se.authgatewayapi.repository.RoleRepository
import hse.se.authgatewayapi.security.JwtTokenProvider
import io.jsonwebtoken.Jwts
import org.springframework.http.server.reactive.ServerHttpRequest

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class AuthService(
    private val userService: UserService,
    private val authenticationManager: AuthenticationManager,
    private val jwtTokenProvider: JwtTokenProvider
) {

    fun authenticate(request: AuthenticationCredentialsDto): Map<String, String> {
        authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                request.email,
                request.password
            )
        )
        val user = userService.findByEmail(request.email)
        val token = jwtTokenProvider.createToken(user.email, user.roles)
        val response: MutableMap<String, String> = HashMap()
        response["email"] = user.email
        response["token"] = token
        return response
    }

    fun validateToken(token: String): UserDto {
        val auth = jwtTokenProvider.getAuthentication(token)
        SecurityContextHolder.getContext().authentication = auth
        check(jwtTokenProvider.validateToken(token)) {
            "JWT token is expired or invalid"
        }
        val email = jwtTokenProvider.getUsername(token)
        val user = userService.findByEmail(email)
        println(user.toString())
        println(user.roles.firstOrNull())
        return UserDto(
            user.id,
            user.email,
            user.roles.map { it.name }
        )
    }

}