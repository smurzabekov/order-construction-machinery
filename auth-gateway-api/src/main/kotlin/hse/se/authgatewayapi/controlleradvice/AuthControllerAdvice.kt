package hse.se.authgatewayapi.controlleradvice

import hse.se.authgatewayapi.exception.JwtAuthenticationException
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.UnsupportedJwtException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class AuthControllerAdvice: ResponseEntityExceptionHandler()  {

    @ExceptionHandler(value = [(UnsupportedJwtException::class),
        (MalformedJwtException::class),
        (SignatureException::class),
        (ExpiredJwtException::class),
        (IllegalArgumentException::class),
        (JwtAuthenticationException::class)
    ])
    fun handleConflict(exception: Exception, request: WebRequest): ResponseEntity<Any> =
        handleExceptionInternal(
            exception,
            exception.message + "you need to refresh access token",
            HttpHeaders(),
            HttpStatus.BAD_REQUEST,
            request
        )

}