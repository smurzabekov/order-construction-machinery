package hse.se.authgatewayapi.entity

import org.hibernate.Hibernate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.util.*
import javax.persistence.*

@MappedSuperclass
open class BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long = 0

    @CreatedDate
    @Column(name = "created")
    open var created: Date = Date()

    @LastModifiedDate
    @Column(name = "update")
    open var updated: Date = Date()

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    open var status: Status = Status.ACTIVE

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as BaseEntity

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , created = $created , updated = $updated , status = $status )"
    }

}