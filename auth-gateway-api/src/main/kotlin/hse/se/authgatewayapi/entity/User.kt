package hse.se.authgatewayapi.entity

import org.hibernate.Hibernate
import javax.persistence.*

@Entity
@Table(name = "user_info")
open class User(
    @Column(name = "username", nullable = false, unique = true)
    open var username: String = "",

    @Column(name = "first_name", nullable = false)
    open var firstName: String = "",

    @Column(name = "last_name")
    open var lastName: String = "",

    @Column(name = "email", nullable = false, unique = true)
    open var email: String,

    @Column(name = "password", nullable = false)
    open var password: String

    ) : BaseEntity() {

    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_roles",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")]
    )
    open lateinit var roles: MutableList<Role>

   /* @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinTable(
        name = "producer_info",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")]
    )
    open lateinit var producerInfo: MutableList<ProducerInfo>?

    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinTable(
        name = "consumer_info",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")]
    )
    open lateinit var consumerInfo: MutableList<ConsumerInfo>?
*/
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as User

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , created = $created , updated = $updated , status = $status , username = $username , firstName = $firstName , lastName = $lastName , email = $email , password = $password )"
    }

}