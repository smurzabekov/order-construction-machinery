package hse.se.authgatewayapi.entity

enum class Status {
    ACTIVE, NOT_ACTIVE, DELETED
}