package hse.se.authgatewayapi.entity

enum class RoleType(val fullName: String) {
    CONSUMER("ROLE_CONSUMER"),
    PRODUCER("ROLE_PRODUCER")
}