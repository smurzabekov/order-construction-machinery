package hse.se.authgatewayapi.entity

import org.hibernate.Hibernate
import javax.persistence.*

@Entity
@Table(name = "roles")
open class Role(

    @Column(name = "name", nullable = false, unique = true)
    open var name: String,

): BaseEntity() {
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    open lateinit var users: MutableList<User>

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Role

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , created = $created , updated = $updated , status = $status , name = $name )"
    }
}