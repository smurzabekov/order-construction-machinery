package hse.se.authgatewayapi.repository

import hse.se.authgatewayapi.entity.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository : JpaRepository<Role, Long>  {

    fun findByName(name: String): Role


    fun findRolesByUsersId(id: Long): List<Role>
}