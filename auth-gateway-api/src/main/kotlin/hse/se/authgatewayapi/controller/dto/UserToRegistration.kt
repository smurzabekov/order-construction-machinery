package hse.se.authgatewayapi.controller.dto

data class UserToRegistration(
    val username: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val password: String
)
