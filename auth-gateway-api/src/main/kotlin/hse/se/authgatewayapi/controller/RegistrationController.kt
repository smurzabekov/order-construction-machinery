package hse.se.authgatewayapi.controller

import hse.se.authgatewayapi.controller.dto.UserToRegistration
import hse.se.authgatewayapi.entity.RoleType
import hse.se.authgatewayapi.service.RegistrationService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/v1/auth")
class RegistrationController(
    private val service: RegistrationService
) {
    @PostMapping("/signup/consumer")
    fun registerUserConsumer(@RequestBody user: UserToRegistration) = runCatching{
        ResponseEntity.ok(service.registerNewUser(user, RoleType.CONSUMER))
    }.onFailure {
        println(it.message)
        ResponseEntity<String>(it.message, HttpStatus.BAD_REQUEST)
    }

    @PostMapping("/signup/producer")
    fun registerUserProducer(@RequestBody user: UserToRegistration) = runCatching{
        ResponseEntity.ok(service.registerNewUser(user, RoleType.PRODUCER))
    }.onFailure {
        println(it.message)
        ResponseEntity<String>(it.message, HttpStatus.BAD_REQUEST)
    }
}