package hse.se.authgatewayapi.controller.dto

data class AuthenticationCredentialsDto(
    val email: String,
    val password: String
)