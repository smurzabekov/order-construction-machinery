package hse.se.authgatewayapi.controller

import hse.se.authgatewayapi.controller.dto.AuthenticationCredentialsDto
import hse.se.authgatewayapi.service.AuthService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/v1/auth")
class AuthenticationController(
    private val service: AuthService
) {
    @PostMapping("/login")
    fun authenticate(
        @RequestBody request: AuthenticationCredentialsDto
    ) = runCatching {
        ResponseEntity.ok(service.authenticate(request))
    }.onFailure {
        ResponseEntity<String>("Invalid email/password combination", HttpStatus.FORBIDDEN)
    }

    @PostMapping("/validateToken")
    fun validate(
        @RequestParam("token") token: String
    ) = runCatching {
        println("Validate token controller")
        ResponseEntity.ok(service.validateToken(token))
    }.onFailure {
        ResponseEntity<String>("Invalid email/password combination", HttpStatus.FORBIDDEN)
    }


}
