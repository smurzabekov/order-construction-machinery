package hse.se.authgatewayapi.security


import hse.se.authgatewayapi.exception.JwtAuthenticationException
import org.apache.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtTokenFilter(
    private val jwtTokenProvider: JwtTokenProvider
) : OncePerRequestFilter() {

    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, filterChain: FilterChain) {
        val token = jwtTokenProvider.resolveToken(req)
        try {
            if (!token.isNullOrBlank() && jwtTokenProvider.validateToken(token)) {
                val auth = jwtTokenProvider.getAuthentication(token)
                SecurityContextHolder.getContext().authentication = auth
            }
        } catch (e: JwtAuthenticationException) {
            res.status = HttpStatus.SC_UNAUTHORIZED
            res.sendError(HttpStatus.SC_UNAUTHORIZED, e.message)
            SecurityContextHolder.clearContext();
            throw JwtAuthenticationException("JWT token is expired or invalid");
        } catch (e:Exception) {
            res.status = HttpStatus.SC_UNAUTHORIZED
            res.sendError(HttpStatus.SC_UNAUTHORIZED, e.message)
            SecurityContextHolder.clearContext();
            throw JwtAuthenticationException("JWT token is expired or invalid");

        }
        filterChain.doFilter(req, res)
    }
}
