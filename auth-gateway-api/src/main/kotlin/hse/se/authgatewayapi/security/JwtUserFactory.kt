package hse.se.authgatewayapi.security

import hse.se.authgatewayapi.entity.Status
import hse.se.authgatewayapi.entity.User
import org.springframework.security.core.authority.SimpleGrantedAuthority

object JwtUserFactory {
    fun create(user: User): JwtUser {
        return JwtUser(
            user.id,
            user.username,
            user.firstName,
            user.lastName,
            user.email,
            user.password,
            user.roles.map { SimpleGrantedAuthority(it.name) }.toList(),
            user.status == Status.ACTIVE,
            user.updated
        )
    }
}
