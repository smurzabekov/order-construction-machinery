package hse.se.authgatewayapi.security

import hse.se.authgatewayapi.entity.User
import hse.se.authgatewayapi.service.UserService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service


@Service
class JwtUserDetailsService(private val userService: UserService) : UserDetailsService {

    override fun loadUserByUsername(email: String): UserDetails {
        val user: User = userService.findByEmail(email)
        return JwtUserFactory.create(user)
    }
}
