package hse.se.authgatewayapi.security

import hse.se.authgatewayapi.entity.Role
import hse.se.authgatewayapi.exception.JwtAuthenticationException
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest

@Component
class JwtTokenProvider(
    @Value("\${jwt.token.secret}")
    private var secret: String,
    @Value("\${jwt.token.secretRefresh}")
    private var secretRefresh: String,
    @Value("\${jwt.token.expired}")
    private val validityInMs: Long,
    @Value("\${jwt.token.expiredRefresh}")
    private val validityInMsRefresh: Long,
    @Value("\${jwt.token.header}")
    private val authorizationHeader: String,
    @Value("\${jwt.token.refreshHeader}")
    private val refreshHeader: String,
    private val userDetailsService: UserDetailsService
) {

    @PostConstruct
    fun init() {
        secret = Base64.getEncoder().encodeToString(secret.toByteArray())
    }

    fun createToken(username: String, roles: List<Role>): String {
        val claims = Jwts.claims().setSubject(username)
        claims["roles"] = roles.map { it.name }.toList()
        val now = Date()
        val validity = Date(now.time + validityInMs)
        return Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(now)
            .setExpiration(validity)
            .signWith(SignatureAlgorithm.HS256, secret)
            .compact()
    }

    fun getAuthentication(token: String): Authentication {
        val userDetails = userDetailsService.loadUserByUsername(getUsername(token))
        return UsernamePasswordAuthenticationToken(userDetails, "", userDetails.authorities)
    }

    fun getUsername(token: String): String =
        Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body.subject

    fun resolveToken(request: HttpServletRequest): String? {
        val bearerToken: String? = request.getHeader(authorizationHeader)
        return if (bearerToken != null && bearerToken.startsWith("Bearer_")) {
            bearerToken.substring(7, bearerToken.length)
        } else null
    }


    fun validateToken(token: String): Boolean {
        return try {
            val claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token)
            !claims.body.expiration.before(Date())
        } catch (e: JwtException) {
            throw JwtAuthenticationException("JWT token is expired or invalid")
        } catch (e: IllegalArgumentException) {
            throw JwtAuthenticationException("JWT token is expired or invalid")
        }
    }


}