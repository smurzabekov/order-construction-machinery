package hse.se.authgatewayapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class AuthGatewayApiApplication

fun main(args: Array<String>) {
    runApplication<AuthGatewayApiApplication>(*args)
}
