package hse.se.producerprocessing

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class ProducerProcessingApplication

fun main(args: Array<String>) {
    runApplication<ProducerProcessingApplication>(*args)
}
