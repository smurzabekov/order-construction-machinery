package hse.se.producerprocessing.filter

import org.apache.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthoritiesFilter : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        if(request.getHeader("Authorities") != "ROLE_CONSUMER") {
            response.status = HttpStatus.SC_UNAUTHORIZED
            response.sendError(HttpStatus.SC_UNAUTHORIZED)
            throw IllegalAccessException("No authorities to operate")
        }
        filterChain.doFilter(request, response)

    }
}