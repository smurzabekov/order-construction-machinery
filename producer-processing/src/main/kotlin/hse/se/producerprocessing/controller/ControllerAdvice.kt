package hse.se.producerprocessing.controller

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ControllerAdvice: ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [(IllegalAccessException::class)])
    fun handleConflict(exception: IllegalAccessException, request: WebRequest): ResponseEntity<Any> =
        handleExceptionInternal(exception, exception.message, HttpHeaders(), HttpStatus.UNAUTHORIZED, request)

}