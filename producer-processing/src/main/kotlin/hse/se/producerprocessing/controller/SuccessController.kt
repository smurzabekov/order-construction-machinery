package hse.se.producerprocessing.controller

import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/producer/api/v1")
class SuccessController {

    @GetMapping("/success")
    fun success(request: HttpServletRequest) = request.getHeader("Authorities").toString()
}