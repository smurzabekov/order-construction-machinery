
rootProject.name = "order-construction-machinery"

includeBuild("discovery-server")
includeBuild("auth-gateway-api")
includeBuild("producer-processing")
includeBuild("consumer-processing")
includeBuild("gateway-api")

