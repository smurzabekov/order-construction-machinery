package hse.se.gatewayapi.dto

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonProperty

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class UserDto(
    @JsonProperty("id") val id: Long,
    @JsonProperty("email") val email: String?,
    @JsonProperty("roles") val roles: List<String?>?
)
