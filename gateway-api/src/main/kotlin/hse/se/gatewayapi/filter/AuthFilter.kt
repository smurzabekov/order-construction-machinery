package hse.se.gatewayapi.filter

import config.Config
import hse.se.gatewayapi.configuration.properties.AuthProperties
import hse.se.gatewayapi.dto.UserDto
import org.springframework.cloud.gateway.filter.GatewayFilter
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.time.Duration

private const val AUTHORITIES = "Authorities"

@Component
class AuthFilter(
    @Lazy
    val authService: WebClient.Builder,
    val authProps: AuthProperties
) : AbstractGatewayFilterFactory<Config>(Config::class.java) {

    override fun apply(config: Config): GatewayFilter {
        return GatewayFilter { exchange, chain ->
            println(" Auth filter entered")
            val req = exchange.request
            if (!exchange.request.headers.containsKey(HttpHeaders.AUTHORIZATION)) {
                throw RuntimeException("Missing authorization information")
            }

            var bearerToken: String? = req.headers[HttpHeaders.AUTHORIZATION]?.get(0)
            if (!bearerToken.isNullOrBlank() && bearerToken.startsWith("Bearer_")) {
                bearerToken = bearerToken.substring(7, bearerToken.length)
            } else throw RuntimeException("Incorrect authorization structure")
            println(" Auth filter pre request")


            authService.build()
                .post()
                .uri("${authProps.url}?token=${bearerToken}")
                .retrieve()
                .onStatus(HttpStatus.UNAUTHORIZED::equals) {
                    Mono.error<Throwable>(IllegalAccessException("Need to refresh access token UNAUTHORIZED"))
                }
                .onStatus(HttpStatus.INTERNAL_SERVER_ERROR::equals) {
                    Mono.error<Throwable>(IllegalAccessException("Auth error INTERNAL_SERVER_ERROR"))
                }
                .onStatus(HttpStatus.NOT_FOUND::equals) {
                    Mono.error<Throwable>(IllegalAccessException("Auth microservice error NOT_FOUND"))
                }
                .bodyToMono(UserDto::class.java)
                .timeout(Duration.ofMinutes(1))
                .map {
                    println(it)
                    exchange.request.mutate().header(
                        AUTHORITIES,
                        it.roles?.firstOrNull() ?: run{
                           exchange.response.statusCode = HttpStatus.UNAUTHORIZED
                            throw IllegalAccessException("Auth error")
                        }
                    )
                }.flatMap {
                    println(exchange.request.headers[AUTHORITIES])
                    chain.filter(exchange)
                }
        }
    }
}