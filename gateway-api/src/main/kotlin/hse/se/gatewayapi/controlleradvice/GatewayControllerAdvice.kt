package hse.se.gatewayapi.controlleradvice

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class GatewayControllerAdvice {

    @ExceptionHandler(IllegalAccessException::class)
    fun handleConflict(exception: Exception, request: WebRequest): ResponseEntity<Any> =
        ResponseEntity.internalServerError().body(exception.message)
}